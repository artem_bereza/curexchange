package com.example.ain.curexchange.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Banks {


    @SerializedName("Data")
    @Expose
    public List<Bank> data = null;
    @SerializedName("Success")
    @Expose
    public Boolean success;
    @SerializedName("Errors")
    @Expose
    public Object errors;


    public List<Bank> getData() {
        return data;
    }

    public void setData(List<Bank> data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }
}
