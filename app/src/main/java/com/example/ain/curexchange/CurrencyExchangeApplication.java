package com.example.ain.curexchange;

import android.app.Application;
import android.content.Context;

import com.example.ain.curexchange.injection.component.ApplicationComponent;
import com.example.ain.curexchange.injection.component.DaggerApplicationComponent;
import com.example.ain.curexchange.injection.module.ApplicationModule;

public class CurrencyExchangeApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        getComponent().inject(this);
    }

    public static CurrencyExchangeApplication get(Context context) {
        return (CurrencyExchangeApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}
