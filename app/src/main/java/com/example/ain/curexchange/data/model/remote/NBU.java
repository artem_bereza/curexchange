package com.example.ain.curexchange.data.model.remote;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NBU {
    @SerializedName("Currency")
    @Expose
    public String currency;
    @SerializedName("CurrencyName")
    @Expose
    public String currencyName;
    @SerializedName("Value")
    @Expose
    public Double value;
    @SerializedName("Date")
    @Expose
    public String date;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
