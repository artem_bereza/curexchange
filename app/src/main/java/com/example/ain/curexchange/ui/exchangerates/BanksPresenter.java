package com.example.ain.curexchange.ui.exchangerates;

import com.example.ain.curexchange.data.DataManager;
import com.example.ain.curexchange.data.model.remote.Bank;
import com.example.ain.curexchange.data.model.remote.Banks;
import com.example.ain.curexchange.db.orm.dao.CurrencyDaoImpl;
import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.example.ain.curexchange.ui.base.presenter.BasePresenter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class BanksPresenter extends BasePresenter<BanksMvpView> {
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private List<CurrRate> currencyRates = new ArrayList<>();
    private CurrencyDaoImpl mDbHelper;
    private DataManager mDataManager;

    @Inject
    public BanksPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public void getBanksList() {
        if (!isViewAttached()) return;

        currencyRates.clear();

        compositeSubscription.add(mDataManager.getBanks()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Func1<Banks, Observable<Bank>>() {
                    @Override
                    public Observable<Bank> call(Banks banks) {
                        return Observable.from(banks.getData());
                    }
                })
                .subscribe(bank -> {
                    compositeSubscription.add(mDataManager.getCurrency(bank.key)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(bankCurrencies -> {
                                        for (int j = 0; j < bankCurrencies.size(); j++) {

                                            CurrRate currRate = new CurrRate(bank.getKey(), bank.getName(), bankCurrencies.get(j).getCurrency(),
                                                    bankCurrencies.get(j).getSale(), bankCurrencies.get(j).getBuy(),
                                                    bankCurrencies.get(j).getSaleDelta(), bankCurrencies.get(j).getBuyDelta());

                                            try {
                                                mDbHelper.updateList(currRate);
                                            } catch (SQLException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, throwable -> getMvpView().afterFailedRequest(throwable.getMessage()),
                                    () -> {
                                        try {
                                            getMvpView().afterSuccessGetCurrency(mDbHelper.getListCurrencyUsd());
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }
                                    }));
                }));
    }


    public void getCurrencyNbu() {
        compositeSubscription.add(mDataManager.getNbu()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nbus -> {
                    CurrRate currRate;
                    for (int i = 0; i < nbus.size(); i++) {
                        currRate = new CurrRate("nbu", "НБУ", nbus.get(i).getCurrency(), 0, nbus.get(i).getValue(), 0, 0);
                        try {
                            mDbHelper.updateList(currRate);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    public void getCurrencyBlackMarket() {
        compositeSubscription.add(mDataManager.getBlackMarket()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bankCurrencies -> {
                    CurrRate currRate;
                    for (int i = 0; i < bankCurrencies.size(); i++) {
                        currRate = new CurrRate("black-market", "Чёрный рынок", bankCurrencies.get(i).getCurrency(),
                                bankCurrencies.get(i).getBuy(), bankCurrencies.get(i).getSale(),
                                bankCurrencies.get(i).getBuyDelta(), bankCurrencies.get(i).getSaleDelta());
                        try {
                            mDbHelper.updateList(currRate);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }


    public void getCurrencyMejbank() {
        compositeSubscription.add(mDataManager.getMejbank()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bankCurrencies -> {
                    CurrRate currRate;
                    for (int i = 0; i < bankCurrencies.size(); i++) {
                        currRate = new CurrRate("mejbank", "Межбанк", bankCurrencies.get(i).getCurrency(),
                                bankCurrencies.get(i).getBuy(), bankCurrencies.get(i).getSale(),
                                bankCurrencies.get(i).getBuyDelta(), bankCurrencies.get(i).getSaleDelta());
                        try {
                            mDbHelper.updateList(currRate);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    public void getCurrencyExchange() {
        compositeSubscription.add(mDataManager.getExchange()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bankCurrencies -> {
                    CurrRate currRate;
                    for (int i = 0; i < bankCurrencies.size(); i++) {
                        currRate = new CurrRate("exchange", "Средне-банковский курс", bankCurrencies.get(i).getCurrency(),
                                bankCurrencies.get(i).getBuy(), bankCurrencies.get(i).getSale(),
                                bankCurrencies.get(i).getBuyDelta(), bankCurrencies.get(i).getSaleDelta());
                        try {
                            mDbHelper.updateList(currRate);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    @Override
    public void attachView(BanksMvpView banksMvpView) {
        super.attachView(banksMvpView);
    }

    public void setDbHelper(CurrencyDaoImpl mDbHelper) {
        this.mDbHelper = mDbHelper;
    }

    @Override
    public void detachView() {
        compositeSubscription.clear();
        super.detachView();
    }
}
