package com.example.ain.curexchange.injection.component;

import com.example.ain.curexchange.injection.PerActivity;
import com.example.ain.curexchange.injection.module.ActivityModule;
import com.example.ain.curexchange.ui.exchangerates.MainActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
}
