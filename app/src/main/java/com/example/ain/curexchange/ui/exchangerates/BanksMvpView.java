package com.example.ain.curexchange.ui.exchangerates;

import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.example.ain.curexchange.ui.base.view.BaseMvpView;

import java.util.List;


public interface BanksMvpView extends BaseMvpView<BanksPresenter> {

    void afterFailedRequest(String message);

    void afterSuccessGetCurrency(List<CurrRate> currencyRates);
}
