package com.example.ain.curexchange.db.orm.dao;

import com.example.ain.curexchange.db.orm.model.CurrRate;

import java.sql.SQLException;
import java.util.List;


public interface CurrencyDao {

    public void createCurrency(CurrRate currRate) throws SQLException;

    public CurrRate getCurrency(String key) throws SQLException;

    public void updateCurrency(CurrRate currRate) throws SQLException;

    public void updateList(CurrRate currRate) throws SQLException;

    public void deleteAll() throws SQLException;

    public List<CurrRate> getAllCurrency() throws SQLException;

    public List<CurrRate> getListCurrencyUsd() throws SQLException;

    public List<CurrRate> getListCurrencyEur() throws SQLException;

    public List<CurrRate> getListCurrencyRub() throws SQLException;

    public List<CurrRate> searchBanks(String searchKey) throws SQLException;

}
