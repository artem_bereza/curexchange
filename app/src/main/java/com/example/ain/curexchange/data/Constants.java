package com.example.ain.curexchange.data;


public class Constants {

    public static final class HTTP {
        public static final String BASE_URL = "http://api.cashex.com.ua/api/v1/";
        public static final String LOCALE_RU = "?locale=ru";
        public static final String LOCALE_UA = "?locale=ua";
        public static final String LOCALE_EN = "?locale=en";
    }

    public static final class DATABASE {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "currencyExchangeDb";

        public static final String TABLE_CURRENCYRATE = "currency_rate";
        public static final String KEY_CURRENCYRATE_ID = "_id";
        public static final String KEY_CURRENCYRATE_DATE = "date";
        public static final String KEY_CURRENCYRATE_BANK = "bank";
        public static final String KEY_CURRENCYRATE_BANK_KEY = "bank_key";
        public static final String KEY_CURRENCYRATE_CURRENCY = "currency";
        public static final String KEY_CURRENCYRATE_ASK = "ask";
        public static final String KEY_CURRENCYRATE_BID = "bid";
        public static final String KEY_CURRENCYRATE_ASK_DIFF = "ask_diff";
        public static final String KEY_CURRENCYRATE_BID_DIFF = "bid_diff";


    }
}
