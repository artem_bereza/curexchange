package com.example.ain.curexchange.ui.exchangerates;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ain.curexchange.R;
import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.example.ain.curexchange.ui.bankitem.BankActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BanksAdapter extends RecyclerView.Adapter<BanksAdapter.ExchangeRatesHolder> {


    private List<CurrRate> data = new ArrayList<>();

    @Inject
    public BanksAdapter() {
    }

    @Override
    public ExchangeRatesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_fragment, parent, false);
        return new ExchangeRatesHolder(view);
    }

    @Override
    public void onBindViewHolder(ExchangeRatesHolder holder, int position) {
        final CurrRate item = data.get(position);
        if (item != null) {
            holder.bankTitle.setText(item.getBankName());
            holder.currAsk.setText(String.valueOf(item.getAsk()));
            holder.currBid.setText(String.valueOf(item.getBid()));
            holder.currAskDiff.setText(String.valueOf(item.getAskDiff()));
            holder.currBidDiff.setText(String.valueOf(item.getBidDiff()));
            holder.cardView.setOnClickListener(v -> {
                Context context = v.getContext();
                Intent intent = new Intent(context, BankActivity.class);
                intent.putExtra("key", item.getBankKey());
                context.startActivity(intent);
            });
        }
    }

    public void setData(List<CurrRate> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ExchangeRatesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.banklist_cardview)
        CardView cardView;
        @BindView(R.id.bank_title)
        TextView bankTitle;
        @BindView(R.id.bank_ask)
        TextView currAsk;
        @BindView(R.id.bank_bid)
        TextView currBid;
        @BindView(R.id.bank_ask_diff)
        TextView currAskDiff;
        @BindView(R.id.bank_bid_diff)
        TextView currBidDiff;

        public ExchangeRatesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
