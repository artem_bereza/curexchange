package com.example.ain.curexchange.ui.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ain.curexchange.ui.base.presenter.BasePresenter;
import com.example.ain.curexchange.ui.base.view.BaseFragmentView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Mann on 02.12.2016.
 */

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseFragmentView<P> {

    protected Unbinder mUnbinder;
    private P mPresenter;

    @Override
    public P getPresenter() {
        return mPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = initPresenter();
        mPresenter.attachView(this);
        onViewReady(savedInstanceState);
    }

    protected abstract void onViewReady(@Nullable Bundle savedInstanceState);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }
}
