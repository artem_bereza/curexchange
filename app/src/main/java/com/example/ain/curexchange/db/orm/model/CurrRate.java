package com.example.ain.curexchange.db.orm.model;


import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

import static com.example.ain.curexchange.data.Constants.DATABASE.*;

@DatabaseTable(tableName = TABLE_CURRENCYRATE)
public class CurrRate {

    @DatabaseField(generatedId = true, columnName = KEY_CURRENCYRATE_ID)
    private int _id;

    @DatabaseField(columnName = KEY_CURRENCYRATE_BANK_KEY)
    private String bankKey;

    @DatabaseField(dataType = DataType.DATE, columnName = KEY_CURRENCYRATE_DATE)
    private Date date;
    @DatabaseField(columnName = KEY_CURRENCYRATE_BANK)
    private String bankName;
    @DatabaseField(columnName = KEY_CURRENCYRATE_CURRENCY)
    private String currencyName;
    @DatabaseField(columnName = KEY_CURRENCYRATE_ASK)
    private double ask;
    @DatabaseField(columnName = KEY_CURRENCYRATE_BID)
    private double bid;
    @DatabaseField(columnName = KEY_CURRENCYRATE_ASK_DIFF)
    private double askDiff;
    @DatabaseField(columnName = KEY_CURRENCYRATE_BID_DIFF)
    private double bidDiff;


    public CurrRate() {
    }

    public CurrRate(String bankKey, String bankName, String currencyName, double ask, double bid, double askDiff, double bidDiff) {
        this.bankKey = bankKey;
        this.bankName = bankName;
        this.currencyName = currencyName;
        this.ask = ask;
        this.bid = bid;
        this.askDiff = askDiff;
        this.bidDiff = bidDiff;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getBankKey() {
        return bankKey;
    }

    public void setBankKey(String bankKey) {
        this.bankKey = bankKey;
    }

//    public int get_id() {
//        return _id;
//    }
//
//    public void set_id(int _id) {
//        this._id = _id;
//    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public double getAsk() {
        return ask;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getAskDiff() {
        return askDiff;
    }

    public void setAskDiff(double askDiff) {
        this.askDiff = askDiff;
    }

    public double getBidDiff() {
        return bidDiff;
    }

    public void setBidDiff(double bidDiff) {
        this.bidDiff = bidDiff;
    }
}
