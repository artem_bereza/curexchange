package com.example.ain.curexchange.ui.base.view;

import com.example.ain.curexchange.ui.base.presenter.BasePresenter;

/*
* There add methods to be used in other activities
* For example: showToastr(String message), and after that realize this method  in BaseActivity.
 *If you want to use this method, in child activity simply call showToastr("For example);
* */

public interface BaseActivityView<P extends BasePresenter> extends BaseMvpView<P> {

}