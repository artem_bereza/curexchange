package com.example.ain.curexchange.injection.component;

import android.app.Application;
import android.content.Context;

import com.example.ain.curexchange.CurrencyExchangeApplication;
import com.example.ain.curexchange.data.DataManager;
import com.example.ain.curexchange.data.remote.ExchangeAPIService;
import com.example.ain.curexchange.injection.ApplicationContext;
import com.example.ain.curexchange.injection.module.ApplicationModule;
import com.example.ain.curexchange.injection.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
    void inject(CurrencyExchangeApplication application);

    @ApplicationContext
    Context context();
    Application application();
    ExchangeAPIService baseRestService();

    DataManager dataManager();
}
