package com.example.ain.curexchange.db.orm.dao;

import android.content.Context;

import com.example.ain.curexchange.db.orm.model.CurrRate;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.ain.curexchange.data.Constants.DATABASE.KEY_CURRENCYRATE_BANK;
import static com.example.ain.curexchange.data.Constants.DATABASE.KEY_CURRENCYRATE_BANK_KEY;
import static com.example.ain.curexchange.data.Constants.DATABASE.KEY_CURRENCYRATE_CURRENCY;


public class CurrencyDaoImpl implements CurrencyDao {

    private static CurrencyDaoImpl instance;
    private DataHelper helper;


    public CurrencyDaoImpl(Context context) {
        helper = new DataHelper(context);
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new CurrencyDaoImpl(context);
        }
    }

    static public CurrencyDaoImpl getInstance() {
        return instance;
    }

    public DataHelper getHelper() {
        return helper;
    }


    @Override
    public void createCurrency(CurrRate currRate) throws SQLException {
        getHelper().getDao().create(currRate);
    }

    @Override
    public CurrRate getCurrency(String key) throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_CURRENCY, key).get(0);
    }

    public List<CurrRate> getListBankCurrency(String key) throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_BANK_KEY, key);
    }

    @Override
    public void updateCurrency(CurrRate currRate) throws SQLException {
        getHelper().getDao().update(currRate);
    }

    @Override
    public void updateList(CurrRate currRate) throws SQLException {

        List<CurrRate> currRates = getListBankCurrency(currRate.getBankKey());
        Map<String, CurrRate> currRateMap = new HashMap<>();


        if (currRates.size() == 0) {
            getHelper().getDao().create(currRate);
        } else {
            currRateMap.clear();

            for (CurrRate currency : currRates) {
                currRateMap.put(currency.getCurrencyName(), currency);
            }
            CurrRate rate = currRateMap.get(currRate.getCurrencyName());
            if (rate != null && rate.getBankKey().equals(currRate.getBankKey())) {
                currRate.set_id(rate.get_id());
                getHelper().getDao().update(currRate);
            } else {
                getHelper().getDao().create(currRate);
            }
        }
    }

    @Override
    public void deleteAll() throws SQLException {
        getHelper().getDao().delete(getAllCurrency());

    }

    @Override
    public List<CurrRate> getAllCurrency() throws SQLException {
        return getHelper().getDao().queryForAll();
    }

    @Override
    public List<CurrRate> getListCurrencyUsd() throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_CURRENCY, "USD");
    }

    @Override
    public List<CurrRate> getListCurrencyEur() throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_CURRENCY, "EUR");
    }

    @Override
    public List<CurrRate> getListCurrencyRub() throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_CURRENCY, "RUB");
    }

    @Override
    public List<CurrRate> searchBanks(String searchKey) throws SQLException {
        return getHelper().getDao().queryForEq(KEY_CURRENCYRATE_BANK, searchKey);
    }

}
