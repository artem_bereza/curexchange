package com.example.ain.curexchange.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BankCurrencies {

    @SerializedName("Currency")
    @Expose
    public String currency;
    @SerializedName("CurrencyName")
    @Expose
    public String currencyName;
    @SerializedName("Buy")
    @Expose
    public Double buy;
    @SerializedName("Sale")
    @Expose
    public Double sale;
    @SerializedName("BuyDelta")
    @Expose
    public Double buyDelta;
    @SerializedName("SaleDelta")
    @Expose
    public Double saleDelta;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Double getBuy() {
        return buy;
    }

    public void setBuy(Double buy) {
        this.buy = buy;
    }

    public Double getSale() {
        return sale;
    }

    public void setSale(Double sale) {
        this.sale = sale;
    }

    public Double getBuyDelta() {
        return buyDelta;
    }

    public void setBuyDelta(Double buyDelta) {
        this.buyDelta = buyDelta;
    }

    public Double getSaleDelta() {
        return saleDelta;
    }

    public void setSaleDelta(Double saleDelta) {
        this.saleDelta = saleDelta;
    }
}
