package com.example.ain.curexchange.data;

import com.example.ain.curexchange.data.model.remote.BankCurrencies;
import com.example.ain.curexchange.data.model.remote.Banks;
import com.example.ain.curexchange.data.model.remote.NBU;
import com.example.ain.curexchange.data.remote.ExchangeAPIService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class DataManager implements ExchangeAPIService {

    private final ExchangeAPIService exchangeAPIService;

    @Inject
    public DataManager(ExchangeAPIService exchangeAPIService) {
        this.exchangeAPIService = exchangeAPIService;
    }

    public Observable<Banks> getBanks() {
        return exchangeAPIService.getBanks();
    }

    public Observable<List<BankCurrencies>> getCurrency(String key) {
        return exchangeAPIService.getCurrency(key);
    }

    @Override
    public Observable<List<BankCurrencies>> getExchange() {
        return exchangeAPIService.getExchange();
    }

    @Override
    public Observable<List<NBU>> getNbu() {
        return exchangeAPIService.getNbu();
    }

    @Override
    public Observable<List<BankCurrencies>> getMejbank() {
        return exchangeAPIService.getMejbank();
    }

    @Override
    public Observable<List<BankCurrencies>> getBlackMarket() {
        return exchangeAPIService.getBlackMarket();
    }
}
