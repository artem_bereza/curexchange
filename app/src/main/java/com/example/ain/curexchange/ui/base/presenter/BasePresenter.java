package com.example.ain.curexchange.ui.base.presenter;

import com.example.ain.curexchange.ui.base.view.BaseMvpView;

public class BasePresenter<T extends BaseMvpView> implements Presenter<T> {

    private T mMvpView;

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public T getMvpView() {
        return mMvpView;
    }
}