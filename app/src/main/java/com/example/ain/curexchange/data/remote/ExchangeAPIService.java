package com.example.ain.curexchange.data.remote;

import com.example.ain.curexchange.data.model.remote.BankCurrencies;
import com.example.ain.curexchange.data.model.remote.Banks;
import com.example.ain.curexchange.data.model.remote.NBU;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

import static com.example.ain.curexchange.data.Constants.HTTP.LOCALE_RU;


public interface ExchangeAPIService {

    @GET("dictionary/banks" + LOCALE_RU)
    Observable<Banks> getBanks(); //список банков

    @GET("exchange/bank/{key}")
    Observable<List<BankCurrencies>> getCurrency(@Query("key") String key);//курс по банку

    @GET("exchange" + LOCALE_RU)
    Observable<List<BankCurrencies>> getExchange(); //средний курс обмена валют по банкам

    @GET("exchange/nbu" + LOCALE_RU)
    Observable<List<NBU>> getNbu(); //Курс НБУ

    @GET("exchange/mejbank" + LOCALE_RU)
    Observable<List<BankCurrencies>> getMejbank(); //Курс межбанк

    @GET("exchange/black-market" + LOCALE_RU)
    Observable<List<BankCurrencies>> getBlackMarket();//Курс черный рынок

}
