package com.example.ain.curexchange.ui.base.view;

import android.support.annotation.LayoutRes;

import com.example.ain.curexchange.ui.base.presenter.BasePresenter;

public interface BaseMvpView<P extends BasePresenter> {

    @LayoutRes
    int getLayoutResource();

    void injectToComponent();

    P initPresenter();

    P getPresenter();

}