package com.example.ain.curexchange.ui.bankitem;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.ain.curexchange.R;
import com.example.ain.curexchange.db.orm.dao.CurrencyDaoImpl;
import com.example.ain.curexchange.db.orm.model.CurrRate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BankActivity extends AppCompatActivity {
    private CurrencyDaoImpl dbHelper;
    BankAdapter mAdapter;
    String bankKey;
    @BindView(R.id.bank_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.recycle_currencylist)
    RecyclerView mBankList;
    List<CurrRate> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_activity);
        ButterKnife.bind(this);
        dbHelper = new CurrencyDaoImpl(this);
        mBankList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BankAdapter();
        mBankList.setAdapter(mAdapter);
        bankKey = getIntent().getStringExtra("key");
        initToolbar();
        try {
            data = getBanksCurrency(bankKey);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mAdapter.setData(data);


    }

    public List<CurrRate> getBanksCurrency(String key) throws SQLException {
        return dbHelper.getListBankCurrency(key);
    }

    public void initToolbar() {
        mToolbar.setTitle(bankKey);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setNavigationOnClickListener(v -> finish());
    }


}
