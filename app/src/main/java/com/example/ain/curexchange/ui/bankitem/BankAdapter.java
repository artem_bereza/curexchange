package com.example.ain.curexchange.ui.bankitem;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ain.curexchange.R;
import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.example.ain.curexchange.ui.exchangerates.BanksAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BankAdapter extends RecyclerView.Adapter<BankAdapter.BankHolder> {

    List<CurrRate> data = new ArrayList<>();

    @Override
    public BankHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_fragment, parent, false);
        return new BankHolder(view);
    }

    @Override
    public void onBindViewHolder(BankHolder holder, int position) {
        CurrRate item = data.get(position);
        if (item != null) {

            holder.currTitle.setText(item.getCurrencyName());
            holder.currAsk.setText(String.valueOf(item.getAsk()));
            holder.currBid.setText(String.valueOf(item.getBid()));
            holder.currAskDiff.setText(String.valueOf(item.getAskDiff()));
            holder.currBidDiff.setText(String.valueOf(item.getBidDiff()));

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<CurrRate> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public static class BankHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.banklist_cardview)
        CardView cardView;
        @BindView(R.id.bank_title)
        TextView currTitle;
        @BindView(R.id.bank_ask)
        TextView currAsk;
        @BindView(R.id.bank_ask_diff)
        TextView currAskDiff;
        @BindView(R.id.bank_bid)
        TextView currBid;
        @BindView(R.id.bank_bid_diff)
        TextView currBidDiff;


        public BankHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
