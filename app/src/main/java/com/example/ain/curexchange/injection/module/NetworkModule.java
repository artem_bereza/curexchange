package com.example.ain.curexchange.injection.module;

import android.content.Context;

import com.example.ain.curexchange.data.Constants;
import com.example.ain.curexchange.data.factory.RxErrorHandlingCallAdapterFactory;
import com.example.ain.curexchange.data.remote.ExchangeAPIService;
import com.example.ain.curexchange.injection.ApplicationContext;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetworkModule {

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        // Log Requests and Responses
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return logging;
    }

    @Provides
    @Singleton
    Cache provideCache(@ApplicationContext Context context) {
        //  Add Cache
        File httpCacheDirectory = new File(context.getCacheDir(), "cache_file");
        return new Cache(httpCacheDirectory, 20 * 1024 * 1024);
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor, @Named("timeout") int timeout) {
        return new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES) // if filed status_code need as statusCode
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
    }


    @Named("BaseRetrofit")
    @Singleton
    @Provides
    Retrofit provideBaseRetrofit(Gson gson, OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.HTTP.BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    ExchangeAPIService provideBaseRestService(@Named("BaseRetrofit") Retrofit retrofit) {
        return retrofit.create(ExchangeAPIService.class);
    }

    @Named("timeout")
    @Provides
    int provideTimeoutConstant() {
        return 30;
    }
}