package com.example.ain.curexchange.ui.exchangerates;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ain.curexchange.R;
import com.example.ain.curexchange.db.orm.dao.CurrencyDaoImpl;
import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.example.ain.curexchange.ui.base.view.BaseActivity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


public class MainActivity extends BaseActivity<BanksPresenter> implements BanksMvpView, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener, android.support.v7.widget.SearchView.OnQueryTextListener {

    @BindView(R.id.recycle_banklist)
    RecyclerView mBankList;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.spinner)
    Spinner spinner;


    @Inject
    BanksPresenter mPresenter;

    @Inject
    BanksAdapter mAdapter;

    private CurrencyDaoImpl dbHelper;

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        initSpinner();
        dbHelper = new CurrencyDaoImpl(this);
        mPresenter.setDbHelper(dbHelper);

        mBankList.setLayoutManager(new LinearLayoutManager(this));
        mBankList.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.getCurrencyNbu();
        mPresenter.getCurrencyMejbank();
        mPresenter.getCurrencyExchange();
        mPresenter.getCurrencyBlackMarket();
        mPresenter.getBanksList();
    }

    @Override
    public void injectToComponent() {
        activityComponent().inject(this);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public BanksPresenter initPresenter() {
        return mPresenter;
    }

    @Override
    public BanksPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onRefresh() {
        mPresenter.getCurrencyNbu();
        mPresenter.getCurrencyMejbank();
        mPresenter.getCurrencyExchange();
        mPresenter.getCurrencyBlackMarket();
        mPresenter.getBanksList();
        spinner.setSelection(0);
    }

    @Override
    public void afterSuccessGetCurrency(List<CurrRate> currencyRates) {
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.setData(currencyRates);
    }

    @Override
    public void afterFailedRequest(String message) {
        mSwipeRefreshLayout.setRefreshing(false);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                try {
                    mAdapter.setData(dbHelper.getListCurrencyUsd());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                try {
                    mAdapter.setData(dbHelper.getListCurrencyEur());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    mAdapter.setData(dbHelper.getListCurrencyRub());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    mAdapter.setData(dbHelper.getListCurrencyUsd());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<CurrRate> items = null;
        switch (spinner.getSelectedItemPosition()) {
            case 0:
                try {
                    items = dbHelper.getListCurrencyUsd();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                try {
                    items = dbHelper.getListCurrencyEur();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    items = dbHelper.getListCurrencyRub();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    items = dbHelper.getListCurrencyUsd();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }

        newText = newText.toLowerCase();
        List<CurrRate> tempList = new ArrayList<>();

        for (CurrRate item : items) {
            String bankName = item.getBankName().toLowerCase();
            if (bankName.contains(newText)) {
                tempList.add(item);
            }
        }
        mAdapter.setData(tempList);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void initSpinner() {
        ArrayAdapter<String> namesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.currency_list));

        namesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(namesAdapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(this);

    }


}