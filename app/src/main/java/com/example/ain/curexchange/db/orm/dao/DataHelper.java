package com.example.ain.curexchange.db.orm.dao;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.ain.curexchange.db.orm.model.CurrRate;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import static com.example.ain.curexchange.data.Constants.DATABASE.DATABASE_NAME;
import static com.example.ain.curexchange.data.Constants.DATABASE.DATABASE_VERSION;


public class DataHelper extends OrmLiteSqliteOpenHelper {


    private Dao<CurrRate, Integer> currRateDao = null;

    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, CurrRate.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, CurrRate.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<CurrRate, Integer> getDao() throws SQLException {
        if (currRateDao == null) {
            currRateDao = getDao(CurrRate.class);
        }
        return currRateDao;
    }

    @Override
    public void close() {
        super.close();
        currRateDao = null;
    }
}

