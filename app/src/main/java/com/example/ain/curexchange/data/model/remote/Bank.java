package com.example.ain.curexchange.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Bank {

    public int bank_id;
    @SerializedName("Key")
    @Expose
    public String key;
    @SerializedName("Name")
    @Expose
    public String name;

    public Bank() {
    }

    public Bank(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public Bank(int bank_id, String key, String name) {
        this.bank_id = bank_id;
        this.key = key;
        this.name = name;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
