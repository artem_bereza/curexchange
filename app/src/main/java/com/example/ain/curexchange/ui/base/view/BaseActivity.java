package com.example.ain.curexchange.ui.base.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.ain.curexchange.CurrencyExchangeApplication;
import com.example.ain.curexchange.injection.component.ActivityComponent;
import com.example.ain.curexchange.injection.component.ConfigPersistentComponent;
import com.example.ain.curexchange.injection.component.DaggerConfigPersistentComponent;
import com.example.ain.curexchange.injection.module.ActivityModule;
import com.example.ain.curexchange.ui.base.presenter.BasePresenter;

import butterknife.ButterKnife;


public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseActivityView<P> {

    private P mPresenter;
    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);

        ConfigPersistentComponent configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .applicationComponent(CurrencyExchangeApplication.get(this).getComponent())
                .build();
        mActivityComponent = configPersistentComponent.activityComponent(new ActivityModule(this));

        injectToComponent();
        mPresenter = initPresenter();
        mPresenter.attachView(this);

        onViewReady(savedInstanceState);
    }

    @Override
    public P getPresenter() {
        return mPresenter;
    }

    public ActivityComponent activityComponent() {
        return mActivityComponent;
    }

    protected abstract void onViewReady(Bundle savedInstanceState);
}
