package com.example.ain.curexchange.ui.base.presenter;


import com.example.ain.curexchange.ui.base.view.BaseMvpView;

public interface Presenter<V extends BaseMvpView> {

    void attachView(V mvpView);

    void detachView();
}
