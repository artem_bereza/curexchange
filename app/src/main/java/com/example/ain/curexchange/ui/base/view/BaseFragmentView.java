package com.example.ain.curexchange.ui.base.view;


import com.example.ain.curexchange.ui.base.presenter.BasePresenter;

/*
* The same as in the @BaseActivityView
* */

public interface BaseFragmentView<P extends BasePresenter> extends BaseMvpView<P> {

}